import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String descLampu = "";
        Boolean status = false;
        int n ;

        System.out.print("Masukan Bilangan : ");
        n = input.nextInt();
        for (int i = 0; i <= n ; i++) {
            if (status){
                status  = false;
            }else {
                status = true;
            }
        }
        if (status) {
            descLampu = "Lampu Hidup";
        }else {
            descLampu = "Lampu Mati";
        }
        System.out.println(descLampu);
    }
}